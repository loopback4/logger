import { Provider } from '@loopback/context';
import { Logger } from 'pino';
export declare class LoggerProvider implements Provider<Logger> {
    logger: any;
    constructor();
    value(): any;
}
