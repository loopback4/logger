"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerProvider = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const pino_1 = tslib_1.__importDefault(require("pino"));
let LoggerProvider = class LoggerProvider {
    constructor() {
        this.logger = (0, pino_1.default)();
    }
    value() {
        return this.logger;
    }
};
LoggerProvider = tslib_1.__decorate([
    (0, core_1.injectable)({ scope: core_1.BindingScope.SINGLETON }),
    tslib_1.__metadata("design:paramtypes", [])
], LoggerProvider);
exports.LoggerProvider = LoggerProvider;
//# sourceMappingURL=loggerProvider.js.map