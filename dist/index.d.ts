import { WebsocketApplication } from "@loopback4/websocket";
import { Component, Binding } from '@loopback/core';
export * from './keys';
export { Logger } from 'pino';
export declare class LoggerComponent implements Component {
    private application;
    bindings: Binding[];
    constructor(application: WebsocketApplication);
}
