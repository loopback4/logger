"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerServiceBindings = void 0;
const core_1 = require("@loopback/core");
var LoggerServiceBindings;
(function (LoggerServiceBindings) {
    LoggerServiceBindings.LOGGER_SERVICE = core_1.BindingKey.create('services.logger');
})(LoggerServiceBindings = exports.LoggerServiceBindings || (exports.LoggerServiceBindings = {}));
//# sourceMappingURL=keys.js.map