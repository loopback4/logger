import { BindingKey } from '@loopback/core';
import { LoggerProvider } from './loggerProvider';
export declare namespace LoggerServiceBindings {
    const LOGGER_SERVICE: BindingKey<LoggerProvider>;
}
