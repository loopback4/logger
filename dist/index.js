"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerComponent = void 0;
const tslib_1 = require("tslib");
const websocket_1 = require("@loopback4/websocket");
const core_1 = require("@loopback/core");
const loggerProvider_1 = require("./loggerProvider");
const keys_1 = require("./keys");
tslib_1.__exportStar(require("./keys"), exports);
let LoggerComponent = class LoggerComponent {
    constructor(application) {
        this.application = application;
        // providers = {
        //     'logger': LoggerProvider
        // };
        this.bindings = [
            core_1.Binding.bind(keys_1.LoggerServiceBindings.LOGGER_SERVICE).toClass(loggerProvider_1.LoggerProvider),
        ];
    }
};
LoggerComponent = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)(core_1.CoreBindings.APPLICATION_INSTANCE)),
    tslib_1.__metadata("design:paramtypes", [websocket_1.WebsocketApplication])
], LoggerComponent);
exports.LoggerComponent = LoggerComponent;
//# sourceMappingURL=index.js.map