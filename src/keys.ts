import {BindingKey} from '@loopback/core';
import {LoggerProvider} from './loggerProvider';

export namespace LoggerServiceBindings {
    export const LOGGER_SERVICE = BindingKey.create<LoggerProvider>('services.logger');
}
