import { Provider } from '@loopback/context';
import {BindingScope, injectable } from '@loopback/core';
import pino, {Logger} from 'pino';

@injectable({scope: BindingScope.SINGLETON})
export class LoggerProvider implements Provider<Logger> {
    logger: any;

    constructor() {
        this.logger = pino();
    }

    value() {
        return this.logger;
    }
}
