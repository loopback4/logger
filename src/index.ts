import {WebsocketApplication} from "@loopback4/websocket";
import { Component, CoreBindings, inject, BindingKey, Binding } from '@loopback/core';
import { LoggerProvider } from './loggerProvider';
import {LoggerServiceBindings} from "./keys";
export * from './keys';
export {Logger} from 'pino';

export class LoggerComponent implements Component {
    // providers = {
    //     'logger': LoggerProvider
    // };
    bindings: Binding[] = [
        Binding.bind(LoggerServiceBindings.LOGGER_SERVICE).toClass(LoggerProvider),
    ]
    constructor(
        @inject(CoreBindings.APPLICATION_INSTANCE) private application: WebsocketApplication,
    ) {
    }
}
